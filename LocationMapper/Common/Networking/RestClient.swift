//
//  RestClient.swift
//  Climaview
//
//  Created by Akshay Phadke on 11/01/19.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import Alamofire
import MobileCoreServices
import SystemConfiguration

class RestClient {
    
    static let baseAPIURL = "https://maps.googleapis.com/maps/api/"
    
    static func request(atPath path: String, forHTTPMethod method: HTTPMethod? = .post, withParams params: [String: Any]? = nil, byShowingProgressHUD shouldShowProgressHUD: Bool = false, forView view: UIView?, completionHandler : ((_ error: NSError?, _ response : Any?, _ data: Data?) -> Void)? = nil) {
        if self.isConnectedToNetwork() == true {
            
            Swift.debugPrint("Complete Path : \(baseAPIURL+path)")
            Swift.debugPrint("Parameters : \(params ?? [String : Any]())")
            
            //            var headers = [String : String]()
            
//            var headers : [String : String] = [
//                "content-type": "application/json"
//            ]
            if shouldShowProgressHUD == true {
                if let _ = view {
                    //To show progress
                }
            }
            
            var encoding : ParameterEncoding!
            if method == .get {
                encoding = URLEncoding.queryString
            }else{
                encoding = JSONEncoding.prettyPrinted
            }
            
            let _ = Alamofire.request(baseAPIURL + path,
                                            method: method!,
                                            parameters: params,
                                            encoding: encoding,
                                            headers: nil)
                .responseJSON { response in
                    if shouldShowProgressHUD == true {
                        if let _ = view {
                            //to hide progress
                        }
                    }
                    switch response.result {
                    case .success(let value):
                        print("Success Response JSON: \(value)")
                        completionHandler!(nil, value, response.data)
                    case .failure:
                        print("Error Response JSON: \(String(describing: response.error))")
                        Utils.presentAlertController(asConfirmationAlert: false, withTitle: "Error", withMessage: "Something went wrong. Please try again later.", okCompletionHandler: {
                                
                        }, cancelCompletionHandler: {
                            
                        })
                        completionHandler!(response.error as NSError?, nil, nil)
                    }
            }
        }else{
            Utils.presentAlertController(asConfirmationAlert: false, withTitle: "Error", withMessage: "Please check your internet connection and try again.", okCompletionHandler: {
                
            }) {
                
            }
        }
    }
    
    static func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let isConnected = (isReachable && !needsConnection)
        
        return isConnected
    }
    
}

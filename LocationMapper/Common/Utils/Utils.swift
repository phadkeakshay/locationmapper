//
//  Utils.swift
//  Climaview
//
//  Created by Akshay Phadke on 11/01/19.
//  Copyright © 2018 Akshay Phadke. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Utils {
    
    static func presentAlertController(asConfirmationAlert: Bool, withTitle title: String?, withMessage message: String, forViewController viewController : UIViewController? = nil, okCompletionHandler : @escaping (() -> ()), cancelCompletionHandler : @escaping (() -> ())) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var title = "Ok"
        if asConfirmationAlert {
            title = "Yes"
        }
        let actionOk = UIAlertAction(title: title, style: .default) { (action) in
            okCompletionHandler()
        }
        alertController.addAction(actionOk)
        if asConfirmationAlert {
            let actionCancel = UIAlertAction(title: "No", style: .default) { (action) in
                cancelCompletionHandler()
            }
            alertController.addAction(actionCancel)
        }
        DispatchQueue.main.async {
            if let viewControllerValue = viewController {
                viewControllerValue.present(alertController, animated: true, completion: nil)
            }
            else {
                guard let navigationController = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as? UINavigationController else { return }
                if let _ = navigationController.topViewController?.presentedViewController {
                    
                }
                else {
                    navigationController.topViewController?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    class func getAttributedString(for string: String, withFont font: UIFont, withForegroundColor foregroundColor: UIColor) -> NSAttributedString {
        return NSAttributedString(string: string, attributes: [NSAttributedString.Key.foregroundColor: foregroundColor, NSAttributedString.Key.font: font as Any])
    }
}


extension MKMapView {
    
    /// When we call this function, we have already added the annotations to the map, and just want all of them to be displayed.
    func fitAll() {
        var zoomRect            = MKMapRect.null;
        for annotation in annotations {
            let annotationPoint = MKMapPoint(annotation.coordinate)
            let pointRect       = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01);
            zoomRect            = zoomRect.union(pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
    
    /// We call this function and give it the annotations we want added to the map. we display the annotations if necessary
    func fitAll(in annotations: [MKAnnotation], andShow show: Bool) {
        var zoomRect:MKMapRect  = MKMapRect.null
        
        for annotation in annotations {
            let aPoint          = MKMapPoint(annotation.coordinate)
            let rect            = MKMapRect(x: aPoint.x, y: aPoint.y, width: 0.1, height: 0.1)
            
            if zoomRect.isNull {
                zoomRect = rect
            } else {
                zoomRect = zoomRect.union(rect)
            }
        }
        if(show) {
            addAnnotations(annotations)
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
    
}

//
//  ViewController.swift
//  LocationMapper
//
//  Created by Akshay Phadke on 11/01/19.
//  Copyright © 2019 Akshay Phadke. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces

class MapViewController: UIViewController {

    var places = [Place]()
    var directionsArray = [MKDirections]()
    var sourcePointAnnotation: MKPointAnnotation?
    var destinationPointAnnotation: MKPointAnnotation?
    var source: GMSPlace?
    var destination: GMSPlace?
    var selectedTextField: UITextField!
    var carPointAnnotation: MKPointAnnotation?
    let textFieldHeightConstant: CGFloat = 40.0
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var sourceLocationTextField: UITextField!
    @IBOutlet var destinationLocationTextField: UITextField!
    @IBOutlet weak var placesTableView: UITableView!
    @IBOutlet weak var placesTableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var sourceLocationTextFieldBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var animateButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    //MARK:- Helper Methods
    private func initialSetup() {
        configurePlacesTableView()
        configureMapView()
        configureTextFields()
    }

    private func configureMapView() {
        mapView.delegate = self
    }
    
    private func configureTextFields() {
        sourceLocationTextField.delegate = self
        destinationLocationTextField.delegate = self
        
        sourceLocationTextField.attributedPlaceholder =  Utils.getAttributedString(for: " Enter Source Location", withFont: UIFont(name: "HelveticaNeue", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0), withForegroundColor: UIColor.darkGray)
        destinationLocationTextField.attributedPlaceholder = Utils.getAttributedString(for: " Enter Destination Location", withFont: UIFont(name: "HelveticaNeue", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0), withForegroundColor: UIColor.darkGray)
        
        sourceLocationTextField.layer.borderWidth = 0.6
        sourceLocationTextField.layer.borderColor = UIColor.darkGray.cgColor
        
        destinationLocationTextField.layer.borderWidth = 0.6
        destinationLocationTextField.layer.borderColor = UIColor.darkGray.cgColor
        
        sourceLocationTextField.addTarget(self, action: #selector(textDidChanged(textField:)), for: .editingChanged)
        destinationLocationTextField.addTarget(self, action: #selector(textDidChanged(textField:)), for: .editingChanged)
    }
    
    private func configurePlacesTableView() {
        placesTableView.dataSource = self
        placesTableView.delegate = self
        placesTableView.rowHeight = UITableView.automaticDimension
        placesTableView.estimatedRowHeight = 80.0
        placesTableView.register(UINib(nibName: PlaceTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: PlaceTableViewCell.cellIdentifier)
        togglePlacesTableViewVisibility(isHidden: true)
    }
    
    private func togglePlacesTableViewVisibility(isHidden: Bool, forTextField textField: UITextField? = nil) {
        DispatchQueue.main.async {
            self.placesTableView.isHidden = isHidden
            if isHidden {
                self.view.sendSubviewToBack(self.placesTableView)
            }
            else {
                self.view.bringSubviewToFront(self.placesTableView)
            }
            guard let textField = textField else { return }
            if textField == self.sourceLocationTextField {
                self.placesTableViewTopConstraint.constant = 0
            }
            else if textField == self.destinationLocationTextField {
                self.placesTableViewTopConstraint.constant = self.sourceLocationTextFieldBottomConstraint.constant + self.textFieldHeightConstant
            }
        }
    }
    
    private func setupAnnotation(for place: GMSPlace, for textField: UITextField) {
        if textField == sourceLocationTextField {
            if let sourcePointAnnotation = sourcePointAnnotation {
                mapView.removeAnnotation(sourcePointAnnotation)
            }
            sourcePointAnnotation = nil
            sourcePointAnnotation = MKPointAnnotation()
            sourcePointAnnotation?.coordinate = place.coordinate
            sourcePointAnnotation?.title = place.name
            sourcePointAnnotation?.subtitle = place.formattedAddress
            mapView.addAnnotation(sourcePointAnnotation!)
        }
        else if textField == destinationLocationTextField {
            if let destinationPointAnnotation = destinationPointAnnotation {
                mapView.removeAnnotation(destinationPointAnnotation)
            }
            destinationPointAnnotation = nil
            destinationPointAnnotation = MKPointAnnotation()
            destinationPointAnnotation?.coordinate = place.coordinate
            destinationPointAnnotation?.title = place.name
            destinationPointAnnotation?.subtitle = place.formattedAddress
            mapView.addAnnotation(destinationPointAnnotation!)
            setupCarAnnotation()
            setDirections()
            animateButton.isEnabled = true; clearButton.isEnabled = true;
        }
    }
    
    private func setupCarAnnotation() {
        guard let sourcePointAnnotation = sourcePointAnnotation else { return }
        if let carPointAnnotation = carPointAnnotation {
            mapView.removeAnnotation(carPointAnnotation)
        }
        carPointAnnotation = nil
        carPointAnnotation = MKPointAnnotation()
        carPointAnnotation?.coordinate = sourcePointAnnotation.coordinate
        carPointAnnotation?.title = ""; carPointAnnotation?.subtitle = "";
        mapView.addAnnotation(carPointAnnotation!)
    }
    
    private func setDirections() {
        
        guard let sourcePointAnnotation = self.sourcePointAnnotation, let destinationPointAnnotation = self.destinationPointAnnotation else { return }
        let sourcePlacemark = MKPlacemark(coordinate: sourcePointAnnotation.coordinate)
        let destinationPlacemark = MKPlacemark(coordinate: destinationPointAnnotation.coordinate)
        
        let directionsRequest = MKDirections.Request()
        directionsRequest.source = MKMapItem(placemark: sourcePlacemark)
        directionsRequest.destination = MKMapItem(placemark: destinationPlacemark)
        directionsRequest.transportType = .automobile
        directionsRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: directionsRequest)
        resetDirections()
        directionsArray.append(directions)
        directions.calculate { (response, error) in
            if error == nil {
                guard let response = response else { return }
                for route in response.routes {
                    self.mapView.addOverlay(route.polyline)
                    self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
                }
            }
            else {
                print("Error Calculating Routes - \(String(describing: error))")
            }
        }
    }
    
    private func resetDirections() {
        mapView.removeOverlays(mapView.overlays)
        let _ = directionsArray.map { $0.cancel() }
        directionsArray.removeAll()
    }
    
    private func reset() {
        resetDirections()
        mapView.removeAnnotations(mapView.annotations)
        sourcePointAnnotation = nil; destinationPointAnnotation = nil; carPointAnnotation = nil;
        places.removeAll()
        sourceLocationTextField.text = nil; destinationLocationTextField.text = nil;
        animateButton.isEnabled = false; clearButton.isEnabled = false;
    }
    
    private func zoomMapView() {
        mapView.fitAll(in: mapView.annotations, andShow: true)
    }
    
    //MARK:- Button Actions
    @IBAction func actionAnimate(_ sender: Any, forEvent event: UIEvent) {
        guard let carPointAnnotation = self.carPointAnnotation, let sourcePointAnnotation = self.sourcePointAnnotation, let destinationPointAnnotation = self.destinationPointAnnotation else { return }
        carPointAnnotation.coordinate = sourcePointAnnotation.coordinate
        animateButton.isEnabled = false; clearButton.isEnabled = false;
        DispatchQueue.main.async {
            UIView.animate(withDuration: 5.0, delay: 1.0, options: .curveEaseIn, animations: {
                carPointAnnotation.coordinate = destinationPointAnnotation.coordinate
            }, completion: { (completed) in
                if completed {
                    self.animateButton.isEnabled = true; self.clearButton.isEnabled = true;
                }
            })
        }
    }
    
    @IBAction func actionClear(_ sender: Any, forEvent event: UIEvent) {
        Utils.presentAlertController(asConfirmationAlert: true, withTitle: "", withMessage: "Are you sure you want to reset?", forViewController: self, okCompletionHandler: {
            self.reset()
        }) {
            
        }
    }
    
    //MARK:- API Calls
    private func getPlaceAutoCompleteResults(for text: String) {
        RestClient.request(atPath: "place/autocomplete/json?input=\(text)&key=AIzaSyAIALWus_W2KF6nQzrf3BMWx8Fg48vfINI&components=country:in", forHTTPMethod: .get, withParams: nil, byShowingProgressHUD: false, forView: self.view) { (error, response, data) in
            if error == nil {
                guard let data = data else { return }
                do {
                    let jsonDecoder = JSONDecoder()
                    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                    let result = try jsonDecoder.decode(PlaceAutoCompleteResult.self, from: data)
                    guard let places = result.predictions else { return }
                    self.places = places
                    DispatchQueue.main.async {
                        self.placesTableView.reloadData()
                    }
                }
                catch {
                    print("Decoding Error - \(error)")
                }
            }
            else {
                print("Error - \(String(describing: error))")
            }
        }
    }
    
    private func getPlaceDetails(for place: Place, for textField: UITextField? = nil) {
        guard let placeId = place.placeId else { return }
        RestClient.request(atPath: "place/details/json?placeid=\(placeId)&key=AIzaSyAIALWus_W2KF6nQzrf3BMWx8Fg48vfINI&components=country:in", forHTTPMethod: .get, withParams: nil, byShowingProgressHUD: false, forView: self.view) { (error, response, data) in
            if error == nil {
                guard let response = response as? [String: Any] else { return }
                guard let result = response["result"] as? [String: Any] else { return }
                guard let geometry = result["geometry"] as? [String: Any] else { return }
                guard let location = geometry["location"] as? [String: Any] else { return }
                guard let textField = textField else { return }
                var description = ""
                if let value = place.description {
                    description = value
                }
                if textField == self.sourceLocationTextField {
                    self.sourceLocationTextField.text = description
                }
                else if textField == self.destinationLocationTextField {
                    self.destinationLocationTextField.text = description
                }
                //self.setupAnnotation(for: place, with: Location(dictionary: location), for: textField)
                DispatchQueue.main.async {
                    textField.resignFirstResponder()
                    if let sourcePointAnnotation = self.sourcePointAnnotation {
                        //self.zoomMapView(around: sourcePointAnnotation.coordinate)
                    }
                }
                self.places.removeAll()
                self.togglePlacesTableViewVisibility(isHidden: true, forTextField: textField)
            }
            else {
                print("Error - \(String(describing: error))")
            }
        }
    }
    
}

//MARK:- UITextFieldDelegate
extension MapViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTextField = textField
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async {
            textField.resignFirstResponder()
            self.togglePlacesTableViewVisibility(isHidden: true, forTextField: textField)
        }
        return true
    }
    
    @objc func textDidChanged(textField: UITextField!) {
        guard let text = textField.text else { togglePlacesTableViewVisibility(isHidden: true, forTextField: textField); return }
        if text != "" {
            togglePlacesTableViewVisibility(isHidden: false, forTextField: textField)
            getPlaceAutoCompleteResults(for: text)
        }
        else {
            togglePlacesTableViewVisibility(isHidden: true, forTextField: textField)
        }
    }
}

//MARK:- GMSAutocompleteViewControllerDelegate
extension MapViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if selectedTextField == sourceLocationTextField {
            sourceLocationTextField.text = place.formattedAddress
            source = place
        }
        else if selectedTextField == destinationLocationTextField {
            destinationLocationTextField.text = place.formattedAddress
            destination = place
        }
        
        self.setupAnnotation(for: place, for: selectedTextField)
        DispatchQueue.main.async {
            self.selectedTextField.resignFirstResponder()
            self.zoomMapView()
        }
        //Adding comment for testing purposes
        self.dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
        //Error, now what?
        //Adding comment on master branch
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotation")
        if annotationView == nil {
            if annotation.title == "" && annotation.subtitle == "" {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
                annotationView?.image = UIImage(named: "car")
            }
            else {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
                annotationView?.canShowCallout = true
            }
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3.0
        return renderer
    }
    
}

//MARK:- UITableViewDataSource
extension MapViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PlaceTableViewCell.cellIdentifier, for: indexPath) as! PlaceTableViewCell
        cell.configure(with: places[indexPath.row])
        return cell
    }
}

//MARK:- UITableViewDelegate
extension MapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if sourceLocationTextField.isFirstResponder {
            getPlaceDetails(for: places[indexPath.row], for: sourceLocationTextField)
        }
        else if destinationLocationTextField.isFirstResponder {
            getPlaceDetails(for: places[indexPath.row], for: destinationLocationTextField)
        }
        
    }
}



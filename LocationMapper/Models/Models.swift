//
//  PlaceModels.swift
//  LocationMapper
//
//  Created by Akshay Phadke on 11/01/19.
//  Copyright © 2019 Akshay Phadke. All rights reserved.
//

import Foundation

class PlaceAutoCompleteResult: Decodable {
    var predictions: [Place]?
    var status: String?
}

class Place: Decodable {
    
    struct Term: Decodable {
        var offset: Int
        var value: String
    }
    
    struct Substring: Decodable {
        var length: Int
        var offset: Int
    }
    
    struct StructuredFormatting: Decodable {
        var mainText: String?
        var mainTextMatchedSubstrings: [Substring]?
        var secondaryText: String?
    }
    
    var description: String?
    var id: String?
    var matchedSubstrings: [Substring]?
    var placeId: String?
    var reference: String?
    var structuredFormatting: StructuredFormatting?
    var terms: [Term]?
    var types: [String]?
    
    enum Keys: String, CodingKey {
        case placeId = "place_id",
        structuredFormatting,
        matchedSubstrings,
        id,
        description,
        reference,
        terms,
        types
    }
}

struct Location {
    var latitude: Double
    var longitude: Double
    
    init(dictionary: [String: Any]) {
        self.latitude = dictionary["lat"] as? Double ?? 0
        self.longitude = dictionary["lng"] as? Double ?? 0
    }
}

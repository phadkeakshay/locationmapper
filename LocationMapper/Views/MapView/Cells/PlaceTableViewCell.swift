//
//  PlaceTableViewCell.swift
//  LocationMapper
//
//  Created by Akshay Phadke on 11/01/19.
//  Copyright © 2019 Akshay Phadke. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    static let cellIdentifier = "PlaceTableViewCell"
    @IBOutlet weak var placeTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        placeTitleLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with place: Place) {
        if let description = place.description {
            placeTitleLabel.text = description
        }
    }
}
